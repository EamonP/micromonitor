var refreshTimer;
function setupRefreshTimer(){
    console.log("Called: [setupRefreshTimer()]")
    refreshTimer = window.setInterval(function(){
        refreshEntries();
        console.log("refresh-default");
    }, 5000);
}

function changeRefreshInterval(newValue) {
    console.log("Called: [changeRefreshInterval(" + newValue + ")]")
    if (newValue == ""){
        newValue = 5;
    }

    window.clearInterval(refreshTimer);
    refreshTimer = window.setInterval(function(){
        refreshEntries();
        console.log("refresh-" + newValue);
    }, 1000 * newValue);
}
