var monitors;

if (!String.format) {
    String.format = function (format) {
        console.log("Called: [String.Format]");
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ?
                args[number] :
                match;
        });
    };
}

function createCollection() {
    console.log("Called: [createCollection()]");
    LDB.clear();
    monitors = new LDB.Collection('Monitors');
}

function addEntry(name, url) {
    console.log("Called: [addEntry(" + name + "," + url + ")]");

    var exprhttp = /http/;

    if (!(exprhttp.test(url))){
        url = "http://" + url;
    }

    var item = {
        name: name,
        url: url
    };

    monitors.save(item, function (_item) {
        console.info('New item:', _item);
    });

    createMonitorDiv(name, url);
    refreshEntry(name, url);
}

function createMonitorDiv(name, url) {
    console.log("Called: [createMonitorDiv(" + name + "," + url + ")]");
    var newWebsite = document.createElement('div');
    newWebsite.classList.add("column", "col-10", "item");
    newWebsite.innerHTML = '<div class="indicator down" id="{0}"></div> {0} - <font size="2">{1}</font> <br> Last Checked: 1min ago';
    newWebsite.innerHTML = String.format(newWebsite.innerHTML, name, url);

    var Columns = document.getElementsByClassName("columns")[1];
    Columns.insertBefore(newWebsite, document.getElementsByClassName("footer")[0]);
}

function loadEntries() {
    console.log("Called: [loadEntries()]");
    monitors.find({}, function (results) {
        for (var i = 0; i < results.length; i++) {
            var entry = results[i];

            createMonitorDiv(entry.name, entry.url);
        }
    });

    refreshEntries();
}

function refreshEntry(name, url) {
    console.log("Called: [refreshEntry(" + name + "," + url + ")]");
    var indicator = document.getElementById(name);
    axios.get(url)
        .then(function (data) {
            if (data.status == 200) {
                indicator.classList.remove("down");
                indicator.classList.add("up");
            } else {
                indicator.classList.remove("up");
                indicator.classList.add("down");
            }
        })
        .catch(function (err) {
            console.error(err);
            indicator.classList.remove("up");
            indicator.classList.add("down");
        });
}

function refreshEntries() {
    console.log("Called: [refreshEntries()]");
    monitors.find({}, function (results) {
        for (var i = 0; i < results.length; i++) {
            var entry = results[i];
            refreshEntry(entry.name, entry.url);
        }
    });
}