var modalAddMonitor;
var regEmail = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

function setupAddMonitorModal() {
    console.log("Called: [setupAddMonitorModal()]");
    // instanciate new modal
    modalAddMonitor = new tingle.modal({
        footer: true,
        stickyFooter: false,
        closeMethods: [],
        onOpen: function () {
            var urlInput = document.getElementsByName("url")[0];
            urlInput.addEventListener('input', function () { isUrl(); });
        }
    });

    // set content
    modalAddMonitor.setContent([
        'Name of Monitor:<br>',
        '<input name="name" class="monitorInput" placeholder="MyWebsite" >',
        '<br /><br />',
        'Url of Monitor:<br>',
        '<input name="url" class="monitorInput" placeholder="http://127.0.0.1/">'
    ].join(''));

    // add a button
    modalAddMonitor.addFooterBtn('Add Monitor', 'tingle-btn tingle-btn--primary', function () {
        var urlInput = document.getElementsByName("url")[0];

        if(regEmail.test(urlInput.value)){
            addEntry(document.getElementsByName("name")[0].value, document.getElementsByName("url")[0].value);
            modalAddMonitor.close();
        }
    });

    // add another button
    modalAddMonitor.addFooterBtn('Cancel', 'tingle-btn tingle-btn--danger', function () {
        // here goes some logic
        modalAddMonitor.close();
    });
}

function openAddMonitorModal() {
    console.log("Called: [openAddMonitorModal()]");
    modalAddMonitor.open();
}

function isUrl() {
    var urlInput = document.getElementsByName("url")[0];

    if (regEmail.test(urlInput.value)) {
        urlInput.style.borderColor = "#D0E6A5";
    }  else if(urlInput.value == "" ){
        urlInput.style.borderColor = "#6272a4";
    }else {
        urlInput.style.borderColor = "#FA897B";
    } 
}